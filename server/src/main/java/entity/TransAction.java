package entity;

import java.rmi.Remote;
import java.util.List;

/**
 * Created by Hi on 7/27/2020.
 */
public interface TransAction extends Remote {

    List<Response> transAction(List<TransActionClass> transActionClasses)throws Exception;
}
