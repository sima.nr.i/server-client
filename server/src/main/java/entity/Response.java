package entity;

import java.io.Serializable;

/**
 * Created by Hi on 7/27/2020.
 */
public class Response implements Serializable{

    private long balance;
    private String customerName;
    private String rscode;

    public String getRscode() {
        return rscode;
    }

    public void setRscode(String rscode) {
        this.rscode = rscode;
    }

    public String getCustomerName() {
        return customerName;
    }
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }
}
