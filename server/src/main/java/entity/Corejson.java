package entity;

import java.util.List;

/**
 * Created by Hi on 7/27/2020.
 */
public class Corejson {
    private String port,outLog;
    private List<Deposit> deposits;
    public List<Deposit> getDeposits() {
        return deposits;
    }
    public void setDeposits(List<Deposit> deposits) {
        this.deposits = deposits;
    }
    public String getPort() {
        return port;
    }
    public void setPort(String port) {
        this.port = port;
    }
    public String getOutLog() {
        return outLog;
    }
    public void setOutLog(String outLog) {
        this.outLog = outLog;
    }

}
