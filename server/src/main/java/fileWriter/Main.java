package fileWriter;

import entity.TransActionImp;
import fileReader.CoreJsonReader;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

/**
 * Created by Hi on 7/27/2020.
 */
public class Main {
    public static void main(String[] args) {
        CallingPort port=new CallingPort();
        try {
            LocateRegistry.createRegistry(Integer.parseInt(port.callPort()));
            TransActionImp transActionImp = new TransActionImp();
            Naming.rebind("transaction", transActionImp);

        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
