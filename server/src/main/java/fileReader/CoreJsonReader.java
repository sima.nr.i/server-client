package fileReader;

import entity.Corejson;
import entity.Deposit;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Hi on 7/27/2020.
 */
public class CoreJsonReader {

    public JSONObject createCoreJson()  {
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("port","1099");
        //create array of deposite
        Map deposit01= new LinkedHashMap(8);
        deposit01.put("customer","Ali Alavi");
        deposit01.put("id","33227781");
        deposit01.put("initialBalance","1000");
        deposit01.put("upperBound","1000000");
        Map deposit02=new LinkedHashMap(4);
        deposit02.put("customer","Reza Rezaei");
        deposit02.put("id","35527439");
        deposit02.put("initialBalance","0");
        deposit02.put("upperBound","0");
        JSONArray jsonArray=new JSONArray();
        jsonArray.add(deposit01);
        jsonArray.add(deposit02);
        jsonObject.put("deposits",jsonArray);
        // jsonObject.put("deposits",deposit02);
        jsonObject.put("outLog","server.out");
        try {


            PrintWriter pw = new PrintWriter("Corejson.json");
            pw.write(jsonObject.toJSONString());
            pw.flush();
            pw.close();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return jsonObject;
    }
    public List<Corejson> parseJsonObject() throws Exception
    {
        JSONObject coreJson=new JSONObject();
        Corejson corejsonClass=new Corejson();
        JSONParser jsonParser = new JSONParser();
        FileReader reader = new FileReader("Corejson.json");
        //Read JSON file
        Object obj = jsonParser.parse(reader);
        coreJson= (JSONObject) obj;
        corejsonClass.setPort((String) coreJson.get("port"));
        corejsonClass.setOutLog((String) coreJson.get("outLog"));
        JSONArray deposits= (JSONArray) coreJson.get("deposits");
        List<Corejson> corejsonList=new ArrayList<Corejson>();
        List<Deposit>depositList=new ArrayList<Deposit>();
        for (Object o:deposits)
        {
            Map mapo=(Map) o;
            Deposit deposit=new Deposit();
            deposit.setCustomer((String) mapo.get("customer"));
            deposit.setId((String)mapo.get("id"));
            deposit.setInitialBalance((String) mapo.get("initialBalance"));
            deposit.setUpperBound((String) mapo.get("upperBound"));
            depositList.add(deposit);
        }
        corejsonClass.setDeposits(depositList);
        corejsonList.add(corejsonClass);
        return corejsonList;
    }


}
