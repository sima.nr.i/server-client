package fileReader;

import entity.TransActionClass;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Created by Hi on 7/22/2020.
 */
public class XMLParser {

    public List<TransActionClass> xmlParser(){
        List<TransActionClass> arraysTransActionClass = new ArrayList<TransActionClass>();
        try {
            File inputFile = new File("src/main/java/resource/Terminal.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = null;
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = null;
            doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();

            Logger logger = Logger.getLogger("MyLog");
            FileHandler fh;
            fh = new FileHandler("C:/Users/Hi/IdeaProjects/client01/src/main/java/resource/MyLogFile.log");
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);

            NodeList nList = doc.getElementsByTagName("transaction");
             arraysTransActionClass = new ArrayList<TransActionClass>();
            for (int temp = 0; temp < nList.getLength(); temp++) {
                TransActionClass transActionClass = new TransActionClass();
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String id = eElement.getAttribute("id");
                    String type = eElement.getAttribute("type");
                    String amount = eElement.getAttribute("amount");
                    String deposit = eElement.getAttribute("deposit");
                    logger.info("Entering inputs");
                    //customerNumber
                    if (id.equals("")) {
                        try {
                            logger.info("id is null");
                            throw new Exception("id ra vard konid");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else
                        transActionClass.setId(Long.parseLong(id));
                    //   deposit.setCustomerNumber(customernumber);
                    if (type.equals("")) {
                        try {
                            logger.info("type is null");
                            throw new Exception("type ra vard konid");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else
                        transActionClass.setType(type);
                    if (amount.equals("")) {
                        try {
                            logger.info("amount is null");
                            throw new Exception("amount ra vard konid");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else
                        transActionClass.setAmount(Long.parseLong(amount));
                    if (deposit.equals("")) {
                        try {
                            logger.info("deposite is null");
                            throw new Exception("deposite ra vard konid");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else
                        transActionClass.setDeposit(Long.parseLong(deposit));
                }
                arraysTransActionClass.add(transActionClass);
            }

        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return arraysTransActionClass;
    }
}