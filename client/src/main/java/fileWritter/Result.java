package fileWritter;

import entity.Response;
import entity.TransAction;
import entity.TransActionClass;
import fileReader.XMLParser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.rmi.Naming;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hi on 7/27/2020.
 */
public class Result {
    public static final String xmlFilePath = "C:\\Users\\Hi\\IdeaProjects\\client01\\src\\main\\java\\resource\\response.xml";

    public void result() throws Exception {

        final TransAction transAction = (TransAction) Naming.lookup("//localhost:/transaction");
        XMLParser xmlParser = new XMLParser();
        List<TransActionClass> transActionClasses = new ArrayList<TransActionClass>();
        transActionClasses = xmlParser.xmlParser();
        final List<Response>[] responses = new List[10];
        final List<TransActionClass> finalTransActionClasses = transActionClasses;
        Thread thread1 = new Thread() {
            @Override
            public void run() {
                try {
                    responses[0] = transAction.transAction(finalTransActionClasses);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        final List<TransActionClass> finalTransActionClasses1 = transActionClasses;
        Thread thread2 = new Thread() {
            @Override
            public void run() {
                try {
                    responses[0] = transAction.transAction(finalTransActionClasses1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        thread1.start();
        thread2.start();
        responses[0] = transAction.transAction(transActionClasses);
        DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
        Document document = documentBuilder.newDocument();
        // root element
        Element root = document.createElement("response");
        document.appendChild(root);
        for (Response response : responses[0]) {
            System.out.println(response.getCustomerName() + "      balance:" + response.getBalance() + "     rscode:" + response.getRscode());
            //response element
            Element seporde = document.createElement("seporde");
            root.appendChild(seporde);
            // name element
            Element customerName = document.createElement("customerName");
            customerName.appendChild(document.createTextNode(response.getCustomerName()));
            seporde.appendChild(customerName);
            //balanceElement
            Element balance = document.createElement("balance");
            balance.appendChild(document.createTextNode(String.valueOf(response.getBalance())));
            seporde.appendChild(balance);
            //rscode
            Element rscode = document.createElement("rscode");
            rscode.appendChild(document.createTextNode(String.valueOf(response.getRscode())));
            seporde.appendChild(rscode);
        }
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource domSource = new DOMSource(document);
        StreamResult streamResult = new StreamResult(new File(xmlFilePath));
        // If you use
        // StreamResult result = new StreamResult(System.out);
        // the output will be pushed to the standard output ...
        // You can use that for debugging
        transformer.transform(domSource, streamResult);
        System.out.println("Done creating XML File");


    }


}
